﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Shop;

namespace ShopTests
{
    [TestFixture]
    class CheckoutTests
    {
        [Test]
        public void Should_ThrowError_No_Data()
        {
            var sut = new Checkout();

            Assert.Throws<InvalidOperationException>(() => sut.Calculate());
        }

        [Test]
        public void Should_Correctly_Sum_Values()
        {
            var sut = new Checkout();
            var data = new List<string>
            {
                "Apple", "Apple", "Orange", "Apple"
            };
            sut.Read(data);
              
            Assert.That(sut.Calculate(), Is.EqualTo(1.45M));
        }

        [Test]
        public void Should_Correctly_Sum_Group_Values()
        {
            var sut = new Checkout();
            var data = new List<string>
            {
                "Apple", "Apple", "Orange", "Orange", "Orange"
            };
            sut.Read(data);

            Assert.That(sut.Calculate(), Is.EqualTo(1.1M));
        }
    }
}
