﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    public class Checkout : ITillReader
    {
        private IList<string> data = null;
        private readonly Dictionary<string, Tuple<decimal, int, int>> prices;

        public Checkout()
        {
            this.prices = new Dictionary<string, Tuple<decimal, int, int>>(StringComparer.OrdinalIgnoreCase)
            {
                { "apple", new Tuple<decimal, int, int>( 0.6M, 2, 1)},
                { "orange", new Tuple<decimal, int, int>(0.25M, 3, 2)}
            };
        }

        public void Read(IList<string> data)
        {
            this.data = data;
        }

        private IDictionary<string, int> ParseValues()
        {
            return this.data
                .GroupBy(p => p)
                .Select(p => new { Key = p.Key, Total = p.Count() })
                .ToDictionary(k => k.Key, v => v.Total, StringComparer.OrdinalIgnoreCase);
        }

        public decimal Calculate()
        {
            if (this.data == null)
            {
                throw new InvalidOperationException();
            }

            return this.ParseValues()
                .Select(p => new
                {
                    Group = p.Value / this.prices[p.Key].Item2,
                    NonGroup = (p.Value % this.prices[p.Key].Item2),
                    HowManyPay = this.prices[p.Key].Item3,
                    Price = this.prices[p.Key].Item1
                })
                .Sum(p =>
                {
                    return (p.Group * p.HowManyPay * p.Price) + p.NonGroup * p.Price;
                });
        }
    }
}
